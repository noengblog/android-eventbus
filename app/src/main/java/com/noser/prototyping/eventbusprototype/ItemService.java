package com.noser.prototyping.eventbusprototype;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.noser.prototyping.eventbusprototype.events.RemoveItemEvent;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.NoSubscriberEvent;
import timber.log.Timber;

public class ItemService extends Service {

    private ItemDiscovery discovery;

    @Override
    public void onCreate() {
        EventBus.getDefault().register(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        discovery = ItemDiscovery.getInstance();
        discovery.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        discovery.cancel();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * EventBus emits this event if no subscribers for a Event are present.
     *
     * @param event
     */
    public void onEventBackgroundThread(NoSubscriberEvent event) {
        Timber.i("No Subscribers present, pausing");
        discovery.pause();
    }

    public void onEventBackgroundThread(RemoveItemEvent e) {
        discovery.modifyList();
    }

}
