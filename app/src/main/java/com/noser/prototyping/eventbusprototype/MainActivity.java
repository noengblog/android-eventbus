package com.noser.prototyping.eventbusprototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.noser.prototyping.eventbusprototype.events.RemoveItemEvent;

import de.greenrobot.event.EventBus;
import timber.log.Timber;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.plant(new Timber.DebugTree());
        setContentView(R.layout.activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.startService(new Intent(this, ItemService.class));
    }

    public void itemClicked(View v) {
        EventBus.getDefault().post(new RemoveItemEvent());
    }

}

