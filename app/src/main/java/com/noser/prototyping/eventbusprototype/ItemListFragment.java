package com.noser.prototyping.eventbusprototype;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noser.prototyping.eventbusprototype.events.UpdatedListEvent;

import de.greenrobot.event.EventBus;


/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class ItemListFragment extends ListFragment {

    private ItemAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter = new ItemAdapter(getActivity());
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, container, false);
    }

    public void onEventMainThread(final UpdatedListEvent e) {
        adapter.itemsUpdated(e.getItemList());
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onResume() {
        EventBus.getDefault().registerSticky(this);
        super.onResume();
    }
}
