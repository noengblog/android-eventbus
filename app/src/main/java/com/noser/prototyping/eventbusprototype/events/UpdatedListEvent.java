package com.noser.prototyping.eventbusprototype.events;

import com.noser.prototyping.eventbusprototype.Item;

import java.util.List;

public class UpdatedListEvent {

    private final List<Item> itemList;

    public UpdatedListEvent(List<Item> itemList) {
        this.itemList = itemList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

}
