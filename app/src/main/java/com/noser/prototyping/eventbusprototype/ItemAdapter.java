package com.noser.prototyping.eventbusprototype;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ItemAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Item> items;

    public ItemAdapter(Activity context) {
        super();
        inflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public Object getItem(int position) {
        return items == null ? null : items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Item item = (Item) getItem(position);
        View viewToUse;

        if (convertView == null) {
            viewToUse = inflater.inflate(R.layout.entry, null);
            holder = new ViewHolder();
            holder.titleText = (TextView) viewToUse.findViewById(R.id.title);
            viewToUse.setTag(holder);
        } else {
            viewToUse = convertView;
            holder = (ViewHolder) viewToUse.getTag();
        }

        holder.titleText.setText(item.title);
        return viewToUse;
    }

    public void itemsUpdated(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView titleText;
    }

}
