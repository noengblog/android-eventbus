package com.noser.prototyping.eventbusprototype;

import com.noser.prototyping.eventbusprototype.events.UpdatedListEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;
import timber.log.Timber;

/**
 * Changes the ItemList in an async way. This is equivalent to e.g. Bluetooth-Discovery.
 */
public class ItemDiscovery {


    private static ItemDiscovery instance;

    private final SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
    private final Timer timer;
    private boolean running;
    private List<Item> list;

    private ItemDiscovery() {
        list = new ArrayList<>();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (running) {
                    addItem();
                }
            }
        }, 0, 5000);
    }

    static ItemDiscovery getInstance() {
        synchronized (ItemDiscovery.class) {
            if (instance == null) {
                Timber.i("Constructing a new ItemDiscovery");
                instance = new ItemDiscovery();
            }
            return instance;
        }
    }

    private void addItem() {
        Timber.i("Adding a Item to the List");
        list.add(new Item(s.format(new Date())));
        itemListChanged();
    }

    public boolean foundItem() {
        return !list.isEmpty();
    }

    public void pause() {
        running = false;
    }

    public void start() {
        running = true;
    }

    public void cancel() {
        timer.cancel();
        timer.purge();
    }

    public void itemListChanged() {
        UpdatedListEvent e = new UpdatedListEvent(list);
        Timber.i("ItemList has changed posting StickyEvent " + e.getClass().getSimpleName());
        EventBus.getDefault().postSticky(e);
    }

    public void modifyList() {
        Timber.i("Removing a Item from the List");
        list.remove(0);
        itemListChanged();
    }
}
