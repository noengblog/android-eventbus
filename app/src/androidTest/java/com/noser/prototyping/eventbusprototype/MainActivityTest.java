package com.noser.prototyping.eventbusprototype;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.noser.prototyping.eventbusprototype.events.UpdatedListEvent;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.greenrobot.event.EventBus;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);
    private ItemDiscoveryIdlingRessource discovery;

    @Before
    public void registerIdlingResources() {
        discovery = new ItemDiscoveryIdlingRessource();
        Espresso.registerIdlingResources(discovery);
    }

    @Test
    public void waitForItemToAppearTest() {
        onView(withId(R.id.item)).check(matches(isDisplayed()));
    }

    @Test
    public void clickTest() {
        // This does currently not work because it waits for a second Item to appear and then
        // complains that there are two items matching R.id.item.
        onView(withId(R.id.item)).perform(click());
        onView(withId(R.id.item)).check(doesNotExist());
    }

    class ItemDiscoveryIdlingRessource implements IdlingResource {

        private ItemDiscovery itemDiscovery = ItemDiscovery.getInstance();
        private ResourceCallback resourceCallback;
        private boolean idle = false;

        ItemDiscoveryIdlingRessource() {
            EventBus.getDefault().register(this);
        }

        public void setBusy() {
            idle = false;
        }

        public void onEvent(UpdatedListEvent e) {
            idle = true;
            resourceCallback.onTransitionToIdle();
        }

        @Override
        public String getName() {
            return "ItemDiscovery";
        }

        @Override
        public boolean isIdleNow() {
            return idle;
        }

        @Override
        public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
            this.resourceCallback = resourceCallback;
        }
    }

}