# Android EventBus Example

The App demonstrates a robust implementation of a Background 
Service announcing Events to a Activity using an EventBus.

## Sequence Diagram

![](app/doc/ItemDiscovery.png)
